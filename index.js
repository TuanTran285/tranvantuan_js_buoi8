const items = document.querySelectorAll(".item");
const btnForm = document.querySelector("#form");
const btnThemSo = document.querySelector("#btn-them-so")
const btnReset = document.querySelector("#btn-reset")
const arrayEl = document.querySelector("#arrayShow");
items.forEach((item) => {
  item.addEventListener("click", function () {
    items.forEach((item) => {
      item.classList.remove("active");
    });
    this.classList.add("active");
  });
});

// nên đặt array bên ngoài bởi vị khi push thì nhận được nhiều số chứ không phải khi click là push vào một mảng khác
let array = [];
function pushNumberinArray(e) {
  let input = document.querySelector("#txt-them-so");
  let inputValue = input.value * 1;
  if(input.value) {
    let contentArray = "";
    array.push(inputValue);
    contentArray += `${inputValue}, `;
    arrayEl.innerHTML += contentArray;
    input.value = "";
    input.focus();
  }else {
    return
  }
}

btnForm.addEventListener("submit", function (e) {
  e.preventDefault();
  pushNumberinArray();
});

btnReset.addEventListener("click", function(e) {
  e.preventDefault()
  array = []
  arrayEl.innerHTML = []
})

const btnEx1 = document.querySelector("#btnEx1");

function tongSoDuong() {
  const resultEx1 = document.querySelector("#resultEx1");
  let sum = 0;
  for (let i = 0; i < array.length; i++) {
    if (array[i] > 0) {
      sum += array[i];
    }
  }
  resultEx1.innerHTML = `Tổng số dương là: ${sum}`;
}

btnEx1.addEventListener("click", function () {
  tongSoDuong();
});

const btnEx2 = document.querySelector("#btnEx2");

function demSoDuong() {
  const resultEx2 = document.querySelector("#resultEx2");
  let count = 0;
  array.forEach((item) => {
    if (item >= 0) {
      count++;
    }
  });
  resultEx2.innerHTML = `Tổng số dương: ${count} `;
}

btnEx2.addEventListener("click", function () {
  demSoDuong();
});

const btnEx3 = document.querySelector("#btnEx3");
function timSoNhoNhat() {
  const resultEx3 = document.querySelector("#resultEx3");
  let minNum = array[0];
  for (let i = 0; i < array.length; i++) {
    let current = array[i];
    if (minNum > current) {
      minNum = current;
    }
  }
  resultEx3.innerHTML = `Số nhỏ nhất: ${minNum} `;
}

btnEx3.addEventListener("click", function () {
  timSoNhoNhat();
});

const btnEx4 = document.querySelector("#btnEx4");
function timSoDuongNhoNhat() {
  const resultEx4 = document.querySelector("#resultEx4");
  // dùng toán tử spead để truyền vào chứ không hàm Math.min nó sẽ không hiểu là có nhiều phần tử
  const minNum = Math.min(...array.filter(x => x > 0))
  console.log(minNum)
  resultEx4.innerHTML = `Số dương nhỏ nhất: ${minNum}`;
}

btnEx4.addEventListener("click", function () {
  timSoDuongNhoNhat();
});

const btnEx5 = document.querySelector("#btnEx5");

function timSoChanCuoiCung() {
  let lastEven = null
  for(let num of array) {
    if(num % 2 === 0) lastEven = num
  }
  return lastEven
}

btnEx5.addEventListener("click", function () {
  const resultEx5 = document.querySelector("#resultEx5");
  let soChanCuoiCung = timSoChanCuoiCung();
  resultEx5.innerHTML = `Số chẵn cuối cùng: ${soChanCuoiCung}`;
});

const btnEx6 = document.querySelector("#btnEx6");

function doiCho() {
  const viTri1 = document.querySelector("#txt-vitri-1").value * 1;
  const viTri2 = document.querySelector("#txt-vitri-2").value * 1;
  const resultEx6 = document.querySelector("#resultEx6");
  if (array[viTri1] !== -1 && array[viTri2] !== -1) {
    [array[viTri1], array[viTri2]] = [array[viTri2], array[viTri1]];
  } else {
    alert("lỗi");
  }
  resultEx6.innerHTML = `Mảng sau khi đổi ${array}`;
}

btnEx6.addEventListener("click", function () {
  doiCho();
});

const btnEx7 = document.querySelector("#btnEx7");

function sortArray(array) {
  // trong đệ uy là phải có điểm dừng nên nếu mảng nó nhỏ hơn 2 thì dừng ngay để không bị đệ quy
  if (array.length < 2) return array;
  const pivot = array[0];
  const left = [];
  const right = [];
  // nếu mà bắt đầu từ vị trí index 0 thì vị trí bắt đầu trở đi là 1
  for (let i = 1; i < array.length; i++) {
    if (array[i] < pivot) {
      left.push(array[i]);
    } else {
      right.push(array[i]);
    }
  }
  return [...sortArray(left), pivot, ...sortArray(right)];
}

btnEx7.addEventListener("click", function () {
  const resultEx7 = document.querySelector("#resultEx7")
  const kq = sortArray(array)
  resultEx7.innerHTML = `Sắp xếp tăng dần: ${kq}`
});

const btnEx8 = document.querySelector("#btnEx8");

function kiemTraSoNguyenTo(n) {
  let isFlag = true;
  if (n < 2) {
    isFlag = false;
  } else if (n === 2) {
    isFlag = true;
  } else if (n % 2 == 0) {
    isFlag = false;
  } else {
    for (let i = 3; i <= Math.sqrt(n); i += 2) {
        if(n % i == 0) {
            isFlag = false;
            return;
        }
    }
  }
  return isFlag;
}
function timSonguyenToDauTien() {
  const reusltEx8 = document.querySelector("#resultEx8");
//   hàm find là hàm tìm kiếm chỉ cần đúng 1 cái đầu tiên là nó sẽ return về
  const firstPrime = array.find(kiemTraSoNguyenTo)
  firstPrime ? reusltEx8.innerHTML = `Số nguyên tố đầu tiên ${firstPrime}` : reusltEx8.innerHTML = -1
}

btnEx8.addEventListener("click", function () {
  timSonguyenToDauTien();
});

const btnEx9 = document.querySelector("#btnEx9");

function demSoNguyen() {
  const resultEx9 = document.querySelector("#resultEx9")
  let count = 0
  for(let i = 0; i<array.length; i++) {
    // có thể dùng hàm Number.isInteger có sẵn trong js
    if(typeof array[i] === "number" && array[i] % 1 == 0) {
      count++
    }
  }
  resultEx9.innerHTML = `Số nguyên: ${count}`
}

btnEx9.addEventListener("click", function () {
  demSoNguyen()
});


const btnEx10 = document.querySelector("#btnEx10")


function soSanhSLAmDuong() {
  const resultEx10 = document.querySelector("#resultEx10")
  let demDuong = 0
  let demAm = 0
  for(let i = 0; i<array.length; i++) {
    if(array[i] > 0) {
      demDuong++
    } else if(array[i] < 0) {
      demAm++
    }
  }
  if(demDuong > demAm)  {
    resultEx10.innerHTML = `Số dương > Số âm`
  }else if(demDuong < demAm) {
    resultEx10.innerHTML = `Số âm > Số dương`
  }else {
    resultEx10.innerHTML = `Số dương = Số âm`
  }

}

btnEx10.addEventListener("click", function() {
    soSanhSLAmDuong()
})